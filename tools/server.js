const Koa = require('koa');
const serve = require('koa-static');
const log4js = require('log4js');

const PORT_UI = 3000;
const app = new Koa();

app.use(serve('./build'));
app.listen(PORT_UI);
const logger = log4js.getLogger();
logger.info(`server at the ${PORT_UI} port has been started`);
