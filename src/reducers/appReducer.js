import appConstants from '../constants/appConstants';

const initialState = {
  appsList: [],
};

const APPS = [
  {
    id: 1, active: true, address: 'Воронцово Поле ул., 3с1', price: 180, rooms: 3, square: 72, photoUrl: 'https://storage.googleapis.com/parser-segment/11/00/00/78/1100007819/1100007819/original/dff81984438b05524973f6385794eca1.jpg', description: 'Сдаю 1-комнатную квартиру. Ремонт обычный, есть все необходимое для комфортного проживания, кухонный гарнитур, стиральная машина, диван. Вставлены стеклопакеты, есть балкон .Благоустроенный зеленый двор, въезд во двор и стоянка под под шлагбаум. 1-2 мин от метро Белорусская пешком.' }, {
    id: 2, active: true, address: 'Большой Трёхсвятительский пер., 14с3', price: 100, rooms: 2, square: 64, photoUrl: 'https://storage.googleapis.com/parser-segment/20/01/20/75/2001207567/2001207567/modified/thumb_700x700_321e06dbfaec0b3983ebcfe020576487.jpg', description: 'Лот 6174. Мария. Предлагается в аренду квартира с отличным ремонтом.Полностью оборудована всей необходимой бытовой техникой и мебелью.' }, {
    id: 3, active: false, address: 'Генерала Карбышева бул., 7к5', price: 45, rooms: 1, square: 42, photoUrl: 'https://storage.googleapis.com/parser-segment/20/00/84/40/2000844063/2000844063/modified/thumb_700x700_24828bb4cf963ecdc40d9a65d016360e.jpg', description: 'Инна.Оперативный показ.Предлагается современная 2-х комнатная квартира на мансардном этаже. Гостиная-студия совмещена с кухней, мебель и бытовая техника в наличии; к спальне прилегает небольшая комната свободного назначения, которая может быть использована под гардеробную или кабинет; имеются 2 санузла с сауной, джакузи и душевой. БЕЗ КОМИССИИ ДЛЯ АРЕНДАТОРА/NO COMISSION. FOR ENGLISH CALL ' }
];


function appReducer(state = initialState, action) {
  switch (action.type) {
    case appConstants.GET_APPS:
      return {
        ...state,
        appsList: APPS,
      };
    default:
      return state;
  }
}

export default appReducer
;
