import React from 'react';
import { connect } from 'react-redux';
import AppartmentItem from './AppartmentItem';
import AppActions from '../actions/appActions';

class AppartmentList extends React.Component {
  constructor() {
    super();
    this.state = {
      searchText: '',
      sortAsc: true,
    };
  }

  componentWillMount() {
    this.props.dispatch(AppActions.fetchApps());
  }

  handleSearch = (evt) => {
    this.setState({
      searchText: evt.target.value,
    });
  };

  render() {
    return (
      <div>
        <div className="apps__header">
          <input
            className="apps__search"
            type="text"
            value={this.state.searchText}
            onChange={this.handleSearch}
            placeholder="Введите адрес"
          />
          <div>
            <button
              className="apps__sort-button"
              disabled={this.state.sortAsk === true}
              onClick={()=>this.setState({
                sortAsc: true,
              })}
            >
              По возрастанию цены
            </button>
            <button
              className="apps__sort-button"
              disabled={this.state.sortAsk === false}
              onClick={()=>this.setState({
                sortAsc: false,
              })}
            >
              По убыванию цены
            </button>
          </div>
        </div>
        <div className="apps-list">
          { this.props.apps.filter(item => {
              return this.state.searchText.length === 0 ||
                item.address.toString().toUpperCase().indexOf(this.state.searchText.toUpperCase()) >=0
            })
            .sort((a, b) => (this.state.sortAsc ? a.price - b.price : b.price - a.price))
            .map(item => (
            <AppartmentItem
              key={item.id}
              {...item}
            />
          ))}
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  apps: state.apps.appsList,
});

export default connect(mapStateToProps)(AppartmentList);
