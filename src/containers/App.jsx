import React from 'react';
import { Provider } from 'react-redux';

import store from '../createStore';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      foo: 'Hello, world!',
    };
  }

  render() {
    return (
      <Provider store={store}>
        { this.props.children }
      </Provider>
    )
  }
}
