import React from 'react';
import { Link } from 'react-router';

function AppartmentItem(props) {
  return (
    <Link to={`/${props.id}`}>
      <div className="bg">
        <img
          className="photo"
          src={props.photoUrl}
          alt={props.address}
          title={props.address}
        />
        <p>{props.address}</p>
        <p>{props.price} т.р.</p>
        <p>{props.rooms} ком.</p>
        <p>{props.square} м2</p>
      </div>
    </Link>
  );
}

export default AppartmentItem;
