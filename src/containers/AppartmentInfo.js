import React from 'react';
import { connect } from 'react-redux';
import AppActions from '../actions/appActions';

class AppartmentInfo extends React.Component {
  componentWillMount() {
    // не успел сделать фетч  по id
    this.props.dispatch(AppActions.fetchApps());
  }

  getApp() {
    return this.props.apps.find(item => (item.id == this.props.params.id));
  }

  render() {
    const app = this.getApp();
    return (
      <div className="bg">
        <img
          className="photo2"
          src={app.photoUrl}
          alt={app.address}
          title={app.address}
        />
        <p>{app.address}</p>
        <p>{app.price} т.р.</p>
        <p>{app.rooms} ком.</p>
        <p>{app.square} м2</p>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  apps: state.apps.appsList,
});

export default connect(mapStateToProps)(AppartmentInfo);