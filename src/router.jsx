import React from 'react';
import { Router as ReactRouter, Route, IndexRoute, hashHistory } from 'react-router';
import App from './containers/App';
import AppartmentList from './containers/AppartmentList';
import AppartmentInfo from './containers/AppartmentInfo';

const Router = () => (
  <ReactRouter history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={AppartmentList} />
      <Route path="/:id" component={AppartmentInfo} />
    </Route>
  </ReactRouter>
);

export default Router;