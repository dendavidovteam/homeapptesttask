import appConstants from '../constants/appConstants';

const fetchApps = () => ({
  type: appConstants.GET_APPS,
});

export default {
  fetchApps,
};
