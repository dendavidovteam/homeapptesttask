import { createStore, combineReducers } from 'redux';
import appReducer from './reducers/appReducer';

const rootReducer = combineReducers({
  apps: appReducer,
});

export default createStore(rootReducer);
