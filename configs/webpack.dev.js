const webpack = require('webpack');

const autoprefixer = require('autoprefixer');

const config = require('./webpack.common');

config.plugins.push(
  /* eslint-disable comma-dangle */
  new webpack.HotModuleReplacementPlugin()
  /* eslint-enable comma-dangle */
);

config.module.rules.push({
  test: /\.scss$/,
  exclude: /node_modules/,
  use: [
    'style-loader',
    'css-loader',
    {
      loader: 'postcss-loader',
      options: {
        plugins: [autoprefixer({
          browsers: [
            'last 3 version',
            'ie >= 8',
          ],
        })],
        sourceMap: true,
      },
    },
    'sass-loader?sourceMap',
  ],
}, {
  test: /\.css/,
  exclude: /node_modules/,
  use: [
    'style-loader',
    'css-loader',
    {
      loader: 'postcss-loader',
      options: {
        plugins: [autoprefixer({
          browsers: [
            'last 3 version',
            'ie >= 8',
          ],
        })],
        sourceMap: true,
      },
    },
  ],
});

config.devServer = {
  port: 3000,
};

module.exports = config;
