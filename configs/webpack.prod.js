const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const config = require('./webpack.common');

config.plugins.push(
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false,
      screw_ie8: true,
      conditionals: true,
      unused: true,
      comparisons: true,
      sequences: true,
      dead_code: true,
      evaluate: true,
      if_return: true,
      join_vars: true,
    },
    output: {
      comments: false,
    },
  }),
  /* eslint-disable comma-dangle */
  new ExtractTextPlugin('style-[hash].css')
  /* eslint-enable comma-dangle */
);

config.module.rules.push({
  test: /\.scss$/,
  use: ExtractTextPlugin.extract({
    use: [
      {
        loader: 'css-loader',
        query: {
          localIdentName: '[hash:8]',
          modules: true,
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: [autoprefixer({
            browsers: [
              'last 3 version',
              'ie >= 8',
            ],
          })],
          sourceMap: true,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          outputStyle: 'compressed',
          sourceMap: false,
          sourceMapContents: false,
        },
      },
    ],
  }),
});

module.exports = config;
